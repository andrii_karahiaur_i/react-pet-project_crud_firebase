import axios from 'axios'

export default axios.create({
    baseURL: 'https://react-crud-b6878.firebaseio.com/',
})