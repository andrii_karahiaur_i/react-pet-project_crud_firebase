export const required = value => {
    if(value) return undefined
    return 'Поле обязательное'
}

export const maxLengthCreator = maxLength => value => {
    if (value && value.length > maxLength) return `Максимальное количество символов ${maxLength}`
    return undefined
}

export const minLengthCreator = minLength => value => {
    if(value && value.length < minLength) return `Минимальное количество символов ${minLength}`
    return undefined
}

export const email = value => {
    return value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? 'Неверные Email адрес'
        : undefined;
}

