import React, {Fragment} from 'react'
import {Field, reduxForm} from "redux-form";
import {email, maxLengthCreator, minLengthCreator, required} from "../../validations";
import {InputField} from "../FormsControls/InputField";
//import axios from "axios";
//import axios_users from "../../axios/axios_users";
import Button from '../Button/index'
import styles from './SingUp.module.scss'
import {auth} from "../../store/actions/auth";
import {connect} from "react-redux";

const maxLength10 = maxLengthCreator(10);
const minLength6 = minLengthCreator(6);

// form
const FormSingUp = props => {

    return(
        <Fragment>
            <form onSubmit={props.handleSubmit}>
                <Field
                    name={"email"}
                    component={InputField}
                    type={"email"}
                    placeholder={"Email"}
                    validate={[required, email]}
                />
                <Field
                    name="firsName"
                    component={InputField}
                    type="text"
                    placeholder="Firs name"
                    validate={[required, maxLength10]}
                />
                <Field
                    name="lastName"
                    component={InputField}
                    type="text"
                    placeholder="Last name"
                    validate={[required, maxLength10]}
                />
                <Field
                    name="password"
                    component={InputField}
                    type="password"
                    placeholder="password"
                    validate={[required, minLength6, maxLength10]}
                />
                <Button type="submit">Регистрация</Button>
            </form>
        </Fragment>
    )
}

// redux form hoc
const ReduxSingUp = reduxForm({
    form: 'singUp'
})(FormSingUp)

// render form
const SingUp = props => {

    const onSubmit = async formData => {
        props.auth(formData.email, formData.password, false, formData)
        // const authData = {
        //     email: formData.email,
        //     password: formData.password,
        //     returnSecureToken: true
        // }
        // try{
        //     // register all info for user in BD
        //     const response = await axios_users.post('users.json', formData)
        //     // register user
        //     await axios.post("https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyCOE6TeSDZlPuGUSVEBxu3tQIbH8XTYDjM", authData)
        //     console.log(response.data)
        // }
        // catch(error){
        //     console.error(error)
        // }
    }

    return (
        <div className={styles.form}>
            <h1>Sing Up</h1>
            <ReduxSingUp onSubmit={onSubmit}/>
        </div>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        'auth': (email,password,isLogin,formData) => dispatch(auth(email,password,isLogin,formData))
    }
}

export default connect(null,mapDispatchToProps)(SingUp)