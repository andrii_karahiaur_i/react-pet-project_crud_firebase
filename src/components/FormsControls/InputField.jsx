import React from 'react'
import styles from './FormControl.module.scss'

export const InputField = ({input, meta, ...props }) => {
    const hasError = meta.touched && meta.error;

    return(
        <div className={styles.formControl  + " " + (hasError ? styles.formControl_error : "")}>
            <label>
                <input
                    {...input}
                    {...props}
                />
                <span>{hasError ? meta.error : null}</span>
            </label>
        </div>
    )
}