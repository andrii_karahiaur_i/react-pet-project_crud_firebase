import React from 'react'
import {connect} from 'react-redux'
import {Field, reduxForm} from 'redux-form'
import Button from "../Button";
import {InputField} from "../FormsControls/InputField";
import styles from './SingIn.module.scss'
//import axios from "axios";
import {auth} from "../../store/actions/auth";
import {email, minLengthCreator, required} from "../../validations";

const minLength6 = minLengthCreator(6);

const FormSingIn = props => {
    return(
        <form onSubmit={props.handleSubmit}>
            <Field
                name="email"
                component={InputField}
                type="email"
                placeholder="Email"
                validate={[required, email]}
            />
            <Field
                name="password"
                component={InputField}
                type="password"
                placeholder="Password"
                validate={[required, minLength6]}
            />
            <Button type="submit">Войти</Button>
        </form>
    )
}

const ReduxSingIn = reduxForm({
    form: "singIn"
})(FormSingIn)

const SingIn = props => {

    const onSubmit = async formData => {
        props.auth(formData.email, formData.password, true, formData)
        // const authData = {
        //     email: formData.email,
        //     password: formData.password,
        //     returnSecureToken: true
        // }
        // try{
        //     const response = await axios.post("https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyCOE6TeSDZlPuGUSVEBxu3tQIbH8XTYDjM", authData)
        //     console.log(response.data)
        // }
        // catch(error){
        //     console.error(error)
        // }
    }

    return (
        <div className={styles.form}>
            <h1>Login</h1>
            <ReduxSingIn onSubmit={onSubmit}/>
        </div>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        'auth': (email, password, isLogin, formData) => dispatch(auth(email, password, isLogin, formData))
    }
}

export default connect(null,mapDispatchToProps)(SingIn)