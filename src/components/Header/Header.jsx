import React, {useState} from 'react'
import styles from './Header.module.scss'
import Drawer from './Drawer/index'
import MenuToggle from "./MenuToggle/index"
import logo from '../../logo.svg';
import {connect} from "react-redux";

const Header = props => {
    const [menu, setMenu] = useState(false)
    const toggleMenuHandler = () => setMenu(!menu)

    const closeMenuHandler = () => {
        setMenu(false)
    }

    return (
        <header className={styles.header}>
            <img src={logo} alt="Logo" />
            <Drawer
                isOpen={menu}
                onClose={closeMenuHandler}
                isAuthenticated={props.isAuthenticated}
            />
            <MenuToggle
                onToggle={toggleMenuHandler}
                isOpen={menu}
            />
        </header>
    )
}

const mapStateToProps = state => {
    return {
        isAuthenticated: !!state.auth.token
    }
}

export default connect(mapStateToProps)(Header)