import React, {Fragment} from 'react';
import {NavLink} from "react-router-dom";
import styles from "./Drower.module.scss";
import BackDrop from "../BackDrop/index";
import {connect} from "react-redux";


const Drower = props => {
    const clsDrower = [styles.Drower]

    let links = [
        {to:"/", label: "Главная", exact: false},
    ];

    if(!props.isOpen) {
        clsDrower.push(styles.close)
    }

    if(props.isAuthenticated) {
        links.push({to:"/logout", label: "Выйти", exact: false})
    }else{
        links.push({to:"/singin", label: "Войти", exact: false});
        links.push({to:"/singup", label: "Авторизация", exact: false})
    }

    return (
        <Fragment>
            <nav className={clsDrower.join(' ')}>
                <ul>
                    {links.map((link,index) => {
                        return (
                            <li key={index}>
                                <NavLink
                                    to={link.to}
                                    exact={link.exact}
                                    activeClassName={styles.active}
                                >
                                    {link.label}
                                </NavLink>
                            </li>
                        )
                    })}
                </ul>
            </nav>
            {props.isOpen ? <BackDrop onClick={props.onClose}/> : null}

        </Fragment>
    )
}

const mapStateToProps = state => {
    return {
        isAuthenticated: !!state.auth.token
    }
}

export default connect(mapStateToProps)(Drower)