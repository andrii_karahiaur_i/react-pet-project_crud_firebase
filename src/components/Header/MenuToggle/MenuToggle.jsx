import React from 'react'
import styles from './MenuToggle.module.scss'

const MenuToggle = props => {
    const clsIcon = ['fa']
    const clsMenu = [styles.MenuToggle]

    if (props.isOpen) {
        clsIcon.push('fa-times')
        clsMenu.push(styles.open)
    }else {
        clsIcon.push('fa-bars')
    }

    return (
        <div className={clsMenu.join(' ')}>
            <i
                className={clsIcon.join(' ')}
                onClick={props.onToggle}
            >
            </i>
        </div>
    )
}

export default MenuToggle;