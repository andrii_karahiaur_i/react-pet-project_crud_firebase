import {combineReducers} from 'redux'
import {reducer as formReducer} from 'redux-form'
import {authSuccess} from "./authReducer";

const reducers = {
    'auth': authSuccess,
    'form': formReducer
}

const reducer = combineReducers(reducers)
export default reducer