import axios_users from '../../axios/axios_users'
import axios from 'axios'
import {AUTH_LOGOUT, AUTH_SUCCESS} from "./actionsType";

export const auth = (email,password,isLogin,formData) => {
    return async dispatch => {
        const authData = {
            email,
            password,
            returnSecureToken: true
        }
        try{
            //url if login
            let url = "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyCOE6TeSDZlPuGUSVEBxu3tQIbH8XTYDjM"

            if(!isLogin){
                url = "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyCOE6TeSDZlPuGUSVEBxu3tQIbH8XTYDjM"
                // register all info for user in BD
                await axios_users.post('users.json', formData)
            }

            const response = await axios.post(url, authData)
            const data = response.data
            // отсчитываем 1 час от полученого токена (3600 сек)
            const expirationDate = new Date(new Date().getTime() + data.expiresIn * 1000 )

            localStorage.setItem('token', data.idToken)
            localStorage.setItem('userId', data.localId)
            localStorage.setItem('expirationDate', expirationDate)

            dispatch(authSuccess(data.idToken))
            dispatch(autoLogout(data.expiresIn))
        }
        catch(error){
            alert('Вы не зарегестрированы !')
            console.error(error)
        }
    }
}

export const authSuccess = (token) => {
    return {
        type: AUTH_SUCCESS,
        token
    }
}

// Автоматически разлогинем через 1 час
export const logout = () => {
    localStorage.removeItem('token')
    localStorage.removeItem('userId')
    localStorage.removeItem('expirationDate')
    return {
        type: AUTH_LOGOUT
    }
}

// Прошел 1 час текущей сессии
export const autoLogout = (time) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout())
        }, time * 1000)
    }
}

export const autoLogin = () => {
    return dispatch => {
        const _token = localStorage.getItem('token')
        if(!_token) {
            dispatch(logout())
        } else {
            const _expirationDate = new Date( localStorage.getItem('expirationDate') )
            if( _expirationDate <= new Date() ) {
                dispatch(logout())
            } else {
                dispatch(authSuccess(_token))
                dispatch(autoLogout((_expirationDate.getTime() - new Date().getTime()) / 1000 ))
            }
        }
    }
}