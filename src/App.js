import React, {useEffect} from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import {connect} from "react-redux";
import Layout from './hoc/Layout/index'
import SingUp from './components/SingUp/index'
import SingIn from "./components/SingIn/index";
import Home from "./components/Home/index";
import Logout from "./components/Logout/index";
import {autoLogin} from "./store/actions/auth";

const App = props => {
    useEffect(() => {
        props.autoLogin()
    },[])

    let routes = (
        <Switch>
            <Route path="/singin" component={SingIn} />
            <Route path="/singup" component={SingUp} />
            <Route path="/" exact component={Home} />
            <Redirect to={'/'}/>
        </Switch>
    );

    if(props.isAuthenticated) {
        routes = (
            <Switch>
                <Route path="/logout" component={Logout} />
                <Route path="/" exact component={Home} />
                <Redirect to={'/'}/>
            </Switch>
        )
    }

    return (
        <Layout>
            {routes}
        </Layout>
    );
}

const mapStateToProps = state => {
    return {
        isAuthenticated: !!state.auth.token // Привели к true/false - если или нет Токена
    }
}

const mapDispatchToProps = dispatch => {
    return {
        'autoLogin': () => dispatch(autoLogin())
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App)
